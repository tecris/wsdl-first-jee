# JAX-WS web service demo (contract first)
Java WSDL web service boilerplate code

**Stack**
 
| *Technology*  | *Version* |
| ------------- | ------------- |
| Java | 8 |
| Maven | 3.3 |
| Docker | 1.9 |
| Wildfly | 10.0.0 |

## Run E2E tests
 - **One liner**
   - `$ mvn -Pcd verify -Dmaven.buildNumber.doCheck=false`

 - **Step-by-step**

  ```
  $ docker run -d -p 8080:8080 -p 9990:9990 casadocker/alpine-wildfly-soap:10.0.0   # start wildfly container
  $ mvn wildfly:deploy                                                              # deploy application
  $ mvn integration-test                                                            # run tests
  ```

### Test with curl
 - `$ ./postRequest.sh`

###  WSDL
  - `http://localhost:8080/scoreboardService/ScoreboardService?wsdl`

### Create tag with maven 
 - `$ mvn -Darguments="-DskipITs" release:prepare`
 - -Darguments="-DskipITs"  - skip integration tests


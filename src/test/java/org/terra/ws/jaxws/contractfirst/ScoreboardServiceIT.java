package org.terra.ws.jaxws.contractfirst;

import static org.junit.Assert.assertEquals;

import java.net.MalformedURLException;

import javax.xml.ws.BindingProvider;

import org.junit.Before;
import org.junit.Test;

public class ScoreboardServiceIT {

    private final static String END_POINT_URL = "http://localhost:8080/scoreboardService/ScoreboardService";

    private ScoreboardServicePortType scoreboardServicePortType;

    @Before
    public void before() throws MalformedURLException {

        ScoreboardService scoreboardService = new ScoreboardService();
        scoreboardServicePortType = scoreboardService.getScoreboardServicePort();

        ((BindingProvider) scoreboardServicePortType).getRequestContext().put(BindingProvider.ENDPOINT_ADDRESS_PROPERTY,
                END_POINT_URL);
    }

    @Test
    public void test() {

        String expectedCompetitionName = "La Tabon";
        String expectedHomeTeam = "Selaru";
        String expectedVisitorTeam = "Ungheni";
        EventType eventType = new EventType();
        eventType.setCompetition(expectedCompetitionName);
        eventType.setHomeTeamName(expectedHomeTeam);
        eventType.setVisitorTeamName(expectedVisitorTeam);
        ScoreboardRequest scoreboardRequest = new ScoreboardRequest();
        scoreboardRequest.setEvent(eventType);
        ScoreboardResponse scoreboardResponse = scoreboardServicePortType.processScoreboard(scoreboardRequest);
        assertEquals(scoreboardResponse.getEvent().getHomeTeamName(), expectedHomeTeam);
        assertEquals(scoreboardResponse.getEvent().getVisitorTeamName(), expectedVisitorTeam);

    }
}
